from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    path(r'', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path(r'', include('src.bahan.urls')),
    path(r'', include('src.satuan.urls')),
    path(r'', include('src.kategori.urls')),
    path(r'', include('src.resep.urls')),
]
