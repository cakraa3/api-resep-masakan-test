# API Resep Masakan

Please read this document to running the project


## Prerequisites

- Python3 (>= v3.8, 3.11.1 is recomended)
- PostgreSQL (>= v10,  v14 is recommended)
- Install virtual environtment (optional)
  https://pipenv.pypa.io/en/latest/


## Restore Database

Restore the database of this project.
There are 2 option restore using sql or tar file that was backuped from PgAdmin


## Installing & Running

After prerequisites has been setup. You have to make the program run.
  - Make sure that database already restored
  - Install dependency library for this project with run this command `pip install -r requirements.txt`
  - Make sure all variables value in your `.env` is yours
  - Run this command to start server `python manage.py runserver`


## Unit Test Running

 - Run this command to start test `python manage.py test src` 


## Endpoint List

 - {HOST_URL}/
 - {HOST_URL}/bahan/
 - {HOST_URL}/bahan/{id}
 - {HOST_URL}/kategori/
 - {HOST_URL}/kategori/{id}
 - {HOST_URL}/resep/?bahan=1,2,3&kategori=1,2,3
 - {HOST_URL}/resep/{id}


## REQUEST SAMPLES

- Create Resep <br>
{
  "nama": "Nasi Goreng",
  "kategori": 3,
  "bahan": [
        {
            "id_bahan": 1,
            "id_satuan": 1,
            "jumlah": 1
        },
        {
            "id_bahan": 2,
            "id_satuan": 2,
            "jumlah": 2
        },
        {
            "id_bahan": 3,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id_bahan": 4,
            "id_satuan": 4,
            "jumlah": 5
        },
        {
            "id_bahan": 5,
            "id_satuan": 4,
            "jumlah": 5
        },
        {
            "id_bahan": 6,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id_bahan": 7,
            "id_satuan": 5,
            "jumlah": 5
        },
        {
            "id_bahan": 8,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id_bahan": 9,
            "id_satuan": 14,
            "jumlah": 0
        }
    ]
}

- Update Resep <br>
{
  "nama": "Nasi Goreng",
  "kategori": 3,
  "bahan": [
        {
            "id": 5,
            "id_bahan": 1,
            "id_satuan": 1,
            "jumlah": 1
        },
        {
            "id": 6,
            "id_bahan": 2,
            "id_satuan": 2,
            "jumlah": 2
        },
        {
            "id": 7,
            "id_bahan": 3,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id": 8,
            "id_bahan": 4,
            "id_satuan": 4,
            "jumlah": 5
        },
        {
            "id": 9,
            "id_bahan": 5,
            "id_satuan": 4,
            "jumlah": 5
        },
        {
            "id": 10,
            "id_bahan": 6,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id": 11,
            "id_bahan": 7,
            "id_satuan": 5,
            "jumlah": 5
        },
        {
            "id": 12,
            "id_bahan": 8,
            "id_satuan": 14,
            "jumlah": 0
        },
        {
            "id": 13,
            "id_bahan": 9,
            "id_satuan": 14,
            "jumlah": 0
        }
    ]
}
