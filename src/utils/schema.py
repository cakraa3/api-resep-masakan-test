from rest_framework import serializers

class BaseQueryParam(serializers.Serializer):
    page = serializers.IntegerField(required=False)
    limit = serializers.IntegerField(required=False)
