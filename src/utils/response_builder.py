class ResponseBuilder:
    def __init__(self, is_pagination=False):
        self.__response = {"code": 200, "message": 'success'}

    @property
    def message(self):
        return self.__response["message"]

    @message.setter
    def message(self, value):
        self.__response["message"] = value

    @property
    def data(self):
        return self.__response["data"]

    @data.setter
    def data(self, value):
        self.__response["data"] = value

    @property
    def pagination(self):
        return self.__response["pagination"]

    @pagination.setter
    def pagination(self, value):
        self.__response["pagination"] = value

    @property
    def status(self):
        return self.__response["status"]

    @status.setter
    def status(self, value):
        if isinstance(value, bool):
            self.__response["status"] = value
        else:
            raise ValueError("invalid value")
    
    @property
    def code(self):
        return self.__response["code"]

    @code.setter
    def code(self, value):
        self.__response["code"] = value

    def add_attribute(self, attribute):
        self.__response[attribute] = None

    def update_value(self, attribute, value):
        if attribute not in self.__response.keys():
            raise KeyError("attribute not found")
        else:
            self.__response[attribute] = value
        return self

    def to_dict(self):
        return self.__response
