from contextlib import contextmanager
from src.utils.response_builder import ResponseBuilder
from rest_framework import status

@contextmanager
def api_exception_handler():
    response = ResponseBuilder()
    try:
        yield response

    except ValueError as error:
        response.code = status.HTTP_400_BAD_REQUEST
        response.status = False
        response.message = str(error)

    except FileExistsError as error:
        response.code = status.HTTP_409_CONFLICT
        response.status = False
        response.message = str(error)

    except FileNotFoundError as error:
        response.code = status.HTTP_404_NOT_FOUND
        response.status = False
        response.message = str(error)

    except Exception as error:
        response.code = status.HTTP_500_INTERNAL_SERVER_ERROR
        response.status = False
        response.message = str(error)
