from rest_framework import serializers
from ..utils.schema import BaseQueryParam

class ResepListQueryParam(BaseQueryParam):
    bahan = serializers.CharField(required=False)
    kategori = serializers.CharField(required=False)


class ResepDetailCreateSchema(serializers.Serializer):
    id_bahan = serializers.IntegerField()
    id_satuan = serializers.IntegerField()
    jumlah = serializers.IntegerField()


class ResepDetailUpdateSchema(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    id_bahan = serializers.IntegerField()
    id_satuan = serializers.IntegerField()
    jumlah = serializers.IntegerField()


class ResepCreateSchema(serializers.Serializer):
    nama = serializers.CharField()
    kategori = serializers.IntegerField()
    bahan = ResepDetailCreateSchema(many=True)


class ResepUpdateSchema(serializers.Serializer):
    nama = serializers.CharField()
    kategori = serializers.IntegerField()
    bahan = ResepDetailUpdateSchema(many=True)

