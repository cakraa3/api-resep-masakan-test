from rest_framework import viewsets, status
from rest_framework.response import Response
from src.utils.exception_handler import api_exception_handler
from django.db.models import Q, Count, F
from .models import ResepModel, ResepDetailModel
from .serializers import ResepSerializer, ResepDetailSerializer
from . import schema
from ..bahan.models import BahanModel
from ..satuan.models import SatuanModel
from drf_spectacular.utils import extend_schema, OpenApiParameter, OpenApiTypes

@extend_schema(methods=['PATCH'], exclude=True)
class ResepViewSet(viewsets.ModelViewSet):
    queryset = ResepModel.objects.order_by('nama')
    serializer_class = ResepSerializer
    permission_classes = []


    @extend_schema(parameters=[schema.ResepListQueryParam])
    def list(self, request):
        """
        Parameter Examples: <br>
        ------
        bahan = 1,2,3,...n <br>
        kategori = 1,2,3,...n <br>
        """
        data = []
        kategori = (
            [x.strip() for x in request.GET.get('kategori').split(',')]
            if request.GET.get('kategori')
            else None
        )
        bahan = (
            [x.strip() for x in request.GET.get('bahan').split(',')]
            if request.GET.get('bahan')
            else None
        )
        limit = request.GET.get('limit', 10)

        with api_exception_handler() as response_builder:
            queryset = self.get_queryset()
            if kategori:
                queryset = queryset.filter(kategori__in=kategori)
            if bahan:
                for query_filter in bahan:
                    queryset = queryset.filter(resepdetailmodel_resep__bahan=query_filter)
                queryset = queryset.annotate(_id=Count(F('id')))

            self.pagination_class.page_size = limit
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.serializer_class(page, many=True)
                data, pagination = self.get_paginated_response(serializer.data)

            # Set Response Builder
            response_builder.data = data
            response_builder.pagination = pagination

        return Response(response_builder.to_dict(), response_builder.code)

    def retrieve(self, request, pk):
        data = []        
        with api_exception_handler() as response_builder:
            queryset = self.get_queryset().filter(id=pk).first()
            if not queryset:
                raise FileNotFoundError('Not found!')
            serializer = ResepDetailSerializer(queryset)
            data = serializer.data

            # Set Response Builder
            response_builder.data = data

        return Response(response_builder.to_dict(), response_builder.code)

    @extend_schema(request=schema.ResepCreateSchema)
    def create(self, request):
        data = {}
        data['nama'] = request.data.get('nama')
        data['kategori'] = request.data.get('kategori')

        with api_exception_handler() as response_builder:
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Create data bahan
            bahan = request.data.get('bahan')
            if isinstance(bahan, list):
                self.create_or_update_bahan(bahan, serializer.data.get('id'))
                resep = ResepModel.objects.filter(id=serializer.data.get('id')).first()
                serializer = ResepDetailSerializer(resep)

            # Set Response Builder
            response_builder.data = serializer.data
            response_builder.code = status.HTTP_201_CREATED

        return Response(response_builder.to_dict(), response_builder.code)

    @extend_schema(request=schema.ResepUpdateSchema)
    def update(self, request, pk):
        data = {}
        with api_exception_handler() as response_builder:
            resep = self.queryset.filter(id=pk).first()
            if not resep:
                raise FileNotFoundError('Resep not found!')

            data['nama'] = request.data.get('nama')
            data['kategori'] = request.data.get('kategori')

            serializer = self.get_serializer(resep, data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            bahan = request.data.get('bahan')
            if isinstance(bahan, list):
                self.create_or_update_bahan(bahan, serializer.data.get('id'))
                resep = ResepModel.objects.filter(id=serializer.data.get('id')).first()
                serializer = ResepDetailSerializer(resep)

            # Set Response Builder
            response_builder.data = serializer.data

        return Response(response_builder.to_dict(), response_builder.code)

    def destroy(self, request, pk):
        with api_exception_handler() as response_builder:
            kategori = self.queryset.filter(id=pk).first()
            if not kategori:
                raise FileNotFoundError('Resep not found!')

            kategori.delete()

        return Response(response_builder.to_dict(), response_builder.code)

    def create_or_update_bahan(self, data, id_resep):
        if not isinstance(data, list):
            raise ValueError('bahan incorrect format!')
        try:
            resep = ResepModel.objects.filter(id=id_resep).first()
            records = []
            records_to_create = []
            records_to_update = []
            for record in data:
                bahan = BahanModel.objects.filter(id=record.get('id_bahan')).first()
                satuan = SatuanModel.objects.filter(id=record.get('id_satuan')).first()
                if not bahan:
                    raise ValueError(f'id_bahan {bahan.id} not found!')
                resep_detail = ResepDetailModel.objects.filter(id=record.get("id")).first()
                if resep_detail and resep_detail.bahan.id != bahan.id:
                    raise ValueError(f'bahan ({resep_detail.id}) not valid!')

                records.append({
                    "id": resep_detail.id if resep_detail is not None else None,
                    "resep": resep,
                    "bahan": bahan,
                    "jumlah": record.get('jumlah'),
                    "satuan": satuan,
                })
            [
                records_to_update.append(record)
                if record["id"] is not None
                else records_to_create.append(record)
                for record in records
            ]
            [record.pop("id") for record in records_to_create]

            records_to_update_id = [ item['id'] for item in records_to_update]
            deleted_records = ResepDetailModel.objects.filter(~Q(id__in=records_to_update_id), resep=resep.id).delete()

            created_records = ResepDetailModel.objects.bulk_create(
                [ResepDetailModel(**values) for values in records_to_create], batch_size=1000
            )

            ResepDetailModel.objects.bulk_update(
                [
                    ResepDetailModel(**values)
                    for values in records_to_update
                ],
                ["bahan", "jumlah", "satuan"],
                batch_size=1000
            )
        except ValueError as e:
            raise ValueError(str(e))
        except Exception as e:
            import traceback
            traceback.print_exc()
            raise ValueError('bahan incorrect format!')
