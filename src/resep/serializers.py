from rest_framework import serializers
from .models import ResepModel, ResepDetailModel


class ResepSerializer(serializers.ModelSerializer):
    created_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    modified_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    kategori_nama = serializers.SerializerMethodField()

    class Meta:
        model = ResepModel
        fields = '__all__'

    def get_kategori_nama(self, obj):
        return obj.kategori.nama

class ResepDetailSerializer(serializers.ModelSerializer):
    created_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    modified_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    kategori_nama = serializers.SerializerMethodField()
    bahan = serializers.SerializerMethodField()

    class Meta:
        model = ResepModel
        fields = '__all__'

    def get_kategori_nama(self, obj):
        return obj.kategori.nama

    def get_bahan(self, obj):
        result = []
        bahan = obj.resepdetailmodel_resep.all()
        for item in bahan:
            result.append({
                'id': item.id,
                'id_bahan': item.bahan.id,
                'id_satuan': item.satuan.id,
                'nama': item.bahan.nama,
                'satuan': item.satuan.nama,
                'jumlah': item.jumlah,
            })
        return result


