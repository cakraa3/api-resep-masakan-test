from rest_framework.test import APITestCase
from rest_framework import status
from src.bahan.models import BahanModel
from src.kategori.models import KategoriModel
from src.satuan.models import SatuanModel
from src.resep.models import ResepModel



class ResepTest(APITestCase):
    
    def init_data(self):
        bahan1 = BahanModel.objects.create(nama='Nasi')
        bahan2 = BahanModel.objects.create(nama='Telur')
        kategori1 = KategoriModel.objects.create(nama='Main course')
        satuan1 = SatuanModel.objects.create(nama='piring', simbol='piring')
        satuan2 = SatuanModel.objects.create(nama='buah', simbol='pcs')
        return bahan1, bahan2, kategori1, satuan1, satuan2

    def init_resep_data(self):
        bahan1, bahan2, kategori1, satuan1, satuan2 = self.init_data()
        return ResepModel.objects.create(nama="Nasi Goreng", kategori=kategori1)

    def test_create_resep(self):
        bahan1, bahan2, kategori1, satuan1, satuan2 = self.init_data()
        url = '/resep/'
        data = {
            "nama": "Nasi Goreng",
            "kategori": kategori1.id,
            "bahan": [
                {
                    "id_bahan": bahan1.id,
                    "id_satuan": satuan1.id,
                    "jumlah": 1
                },
                {
                    "id_bahan": bahan2.id,
                    "id_satuan": satuan2.id,
                    "jumlah": 2
                },
            ]
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_resep_with_incorrect_body(self):
        url = '/resep/'
        data = {}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def test_update_resep(self):
        resep = self.init_resep_data()
        bahan1, bahan2, kategori1, satuan1, satuan2 = self.init_data()

        url = f'/resep/{resep.id}/'
        data = {
            "nama": "Nasi Goreng",
            "kategori": kategori1.id,
            "bahan": [
                {
                    "id_bahan": bahan1.id,
                    "id_satuan": satuan1.id,
                    "jumlah": 1
                },
                {
                    "id_bahan": bahan2.id,
                    "id_satuan": satuan2.id,
                    "jumlah": 2
                }
            ]
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_resep_with_incorrect_id(self):
        url = f'/resep/1/'
        data = {
            'nama': 'Wijen'
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_update_resep_with_incorrect_body(self):
        resep = self.init_resep_data()
        url = f'/resep/{resep.id}/'
        data = {}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_list_resep(self):
        url = f'/resep/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_resep(self):
        resep = self.init_resep_data()
        url = f'/resep/{resep.id}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_resep_with_incorrect_id(self):
        url = f'/resep/1/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_destroy_resep(self):
        resep = self.init_resep_data()
        url = f'/resep/{resep.id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_destroy_resep_with_incorrect_id(self):
        url = f'/resep/1/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
