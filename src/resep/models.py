from django.db import models
from ..kategori import models as kategori
from ..bahan import models as bahan
from ..satuan import models as satuan

class ResepModel(models.Model):

    class Meta:
        db_table = 'resep'

    id = models.AutoField(primary_key=True)
    nama = models.TextField()
    kategori = models.ForeignKey(kategori.KategoriModel, blank=True, null=True, db_column='id_kategori', related_name='%(class)s_kategori', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return '%s' % (self.nama)


class ResepDetailModel(models.Model):

    class Meta:
        db_table = 'resep_detail'

    id = models.AutoField(primary_key=True)
    resep = models.ForeignKey(ResepModel, blank=True, null=True, db_column='id_resep', related_name='%(class)s_resep', on_delete=models.CASCADE)
    bahan = models.ForeignKey(bahan.BahanModel, blank=True, null=True, db_column='id_bahan', related_name='%(class)s_bahan', on_delete=models.PROTECT)
    jumlah = models.FloatField(null=True, blank=True)
    satuan = models.ForeignKey(satuan.SatuanModel, blank=True, null=True, db_column='id_satuan', related_name='%(class)s_satuan', on_delete=models.PROTECT)
    created_date = models.DateTimeField(auto_now_add=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return '%s' % (self.id)
