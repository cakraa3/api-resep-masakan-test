from rest_framework import viewsets, status
from rest_framework.response import Response
from src.utils.exception_handler import api_exception_handler
from .models import BahanModel
from .serializers import BahanSerializer
from . import schema
from drf_spectacular.utils import extend_schema

@extend_schema(methods=['PATCH'], exclude=True)
class BahanViewSet(viewsets.ModelViewSet):
    queryset = BahanModel.objects.order_by('nama')
    serializer_class = BahanSerializer
    permission_classes = []

    @extend_schema(parameters=[schema.BaseQueryParam])
    def list(self, request):
        data = []
        limit = request.GET.get('limit', 10)

        with api_exception_handler() as response_builder:
            queryset = self.get_queryset()
            self.pagination_class.page_size = limit
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.serializer_class(page, many=True)
                data, pagination = self.get_paginated_response(serializer.data)

            # Set Response Builder
            response_builder.data = data
            response_builder.pagination = pagination

        return Response(response_builder.to_dict(), response_builder.code)

    def retrieve(self, request, pk):
        data = []        
        with api_exception_handler() as response_builder:
            queryset = self.get_queryset().filter(id=pk).first()
            if not queryset:
                raise FileNotFoundError('Not found!')
            serializer = self.serializer_class(queryset)
            data = serializer.data

            # Set Response Builder
            response_builder.data = data

        return Response(response_builder.to_dict(), response_builder.code)

    def create(self, request):
        data = {}
        data['nama'] = request.data.get('nama')
        with api_exception_handler() as response_builder:
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Set Response Builder
            response_builder.data = serializer.data
            response_builder.code = status.HTTP_201_CREATED

        return Response(response_builder.to_dict(), response_builder.code)

    def update(self, request, pk):
        data = {}
        with api_exception_handler() as response_builder:
            bahan = self.queryset.filter(id=pk).first()
            if not bahan:
                raise FileNotFoundError('Bahan not found!')

            data['nama'] = request.data.get('nama')
            serializer = self.get_serializer(bahan, data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Set Response Builder
            response_builder.data = serializer.data

        return Response(response_builder.to_dict(), response_builder.code)

    def destroy(self, request, pk):
        with api_exception_handler() as response_builder:
            kategori = self.queryset.filter(id=pk).first()
            if not kategori:
                raise FileNotFoundError('Bahan not found!')

            kategori.delete()

        return Response(response_builder.to_dict(), response_builder.code)
