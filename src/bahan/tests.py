from rest_framework.test import APITestCase
from rest_framework import status
from src.bahan.models import BahanModel

class BahanTest(APITestCase):

    def init_data(self):
        return BahanModel.objects.create(nama='Mentega')

    def test_create_bahan(self):
        url = '/bahan/'
        data = {
            'nama': 'Mentega'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_bahan_with_incorrect_body(self):
        url = '/bahan/'
        data = {}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def test_update_bahan(self):
        bahanObj = self.init_data()
        url = f'/bahan/{bahanObj.id}/'
        data = {
            'nama': 'Wijen'
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_bahan_with_incorrect_id(self):
        url = f'/bahan/1/'
        data = {
            'nama': 'Wijen'
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_update_bahan_with_incorrect_body(self):
        bahanObj = self.init_data()
        url = f'/bahan/{bahanObj.id}/'
        data = {}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_list_bahan(self):
        url = f'/bahan/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_bahan(self):
        bahanObj = self.init_data()
        url = f'/bahan/{bahanObj.id}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_bahan_with_incorrect_id(self):
        url = f'/bahan/1/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_destroy_bahan(self):
        bahanObj = self.init_data()
        url = f'/bahan/{bahanObj.id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_destroy_bahan_with_incorrect_id(self):
        url = f'/bahan/1/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
