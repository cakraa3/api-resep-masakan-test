from django.db import models


class BahanModel(models.Model):

    class Meta:
        db_table = 'bahan'

    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=255)
    created_date = models.DateTimeField(auto_now_add=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return '%s' % (self.nama)
