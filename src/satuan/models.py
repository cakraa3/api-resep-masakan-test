from django.db import models


class SatuanModel(models.Model):

    class Meta:
        db_table = 'satuan'

    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=100)
    simbol = models.CharField(max_length=100, )
    created_date = models.DateTimeField(auto_now_add=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.id)
