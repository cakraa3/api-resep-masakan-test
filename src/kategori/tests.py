from rest_framework.test import APITestCase
from rest_framework import status
from src.kategori.models import KategoriModel

class KategoriTest(APITestCase):

    def init_data(self):
        return KategoriModel.objects.create(nama='Main course')

    def test_create_kategori(self):
        url = '/kategori/'
        data = {
            'nama': 'Main course'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_kategori_with_incorrect_body(self):
        url = '/kategori/'
        data = {}
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def test_update_kategori(self):
        kategoriObj = self.init_data()
        url = f'/kategori/{kategoriObj.id}/'
        data = {
            'nama': 'Dessert'
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_kategori_with_incorrect_id(self):
        url = f'/kategori/1/'
        data = {
            'nama': 'Wijen'
        }
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    
    def test_update_kategori_with_incorrect_body(self):
        kategoriObj = self.init_data()
        url = f'/kategori/{kategoriObj.id}/'
        data = {}
        response = self.client.put(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_list_kategori(self):
        url = f'/kategori/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_kategori(self):
        kategoriObj = self.init_data()
        url = f'/kategori/{kategoriObj.id}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_recieve_kategori_with_incorrect_id(self):
        url = f'/kategori/1/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_destroy_kategori(self):
        kategoriObj = self.init_data()
        url = f'/kategori/{kategoriObj.id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    
    def test_destroy_kategori_with_incorrect_id(self):
        url = f'/kategori/1/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
