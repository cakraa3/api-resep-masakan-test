from rest_framework import viewsets, status
from rest_framework.response import Response
from src.utils.exception_handler import api_exception_handler
from .models import KategoriModel
from .serializers import KategoriSerializer
from . import schema
from drf_spectacular.utils import extend_schema


@extend_schema(methods=['PATCH'], exclude=True)
class KategoriViewSet(viewsets.ModelViewSet):
    queryset = KategoriModel.objects.order_by('nama')
    serializer_class = KategoriSerializer
    permission_classes = []

    @extend_schema(parameters=[schema.BaseQueryParam])
    def list(self, request):
        data = []
        limit = request.GET.get('limit', 10)

        with api_exception_handler() as response_builder:
            queryset = self.get_queryset()
            self.pagination_class.page_size = limit
            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.serializer_class(page, many=True)
                data, pagination = self.get_paginated_response(serializer.data)

            # Set Response Builder
            response_builder.data = data
            response_builder.pagination = pagination

        return Response(response_builder.to_dict(), response_builder.code)

    def retrieve(self, request, pk):
        data = []        
        with api_exception_handler() as response_builder:
            queryset = self.get_queryset().filter(id=pk).first()
            if not queryset:
                raise FileNotFoundError('Not found!')
            serializer = self.serializer_class(queryset)
            data = serializer.data

            # Set Response Builder
            response_builder.data = data

        return Response(response_builder.to_dict(), response_builder.code)

    def create(self, request):
        data = {}
        data['nama'] = request.data.get('nama')

        with api_exception_handler() as response_builder:
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Set Response Builder
            response_builder.data = serializer.data
            response_builder.code = status.HTTP_201_CREATED

        return Response(response_builder.to_dict(), response_builder.code)

    def update(self, request, pk):
        data = {}
        with api_exception_handler() as response_builder:
            kategori = self.queryset.filter(id=pk).first()
            if not kategori:
                raise FileNotFoundError('Kategori not found!')

            data['nama'] = request.data.get('nama')

            serializer = self.get_serializer(kategori, data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Set Response Builder
            response_builder.data = serializer.data

        return Response(response_builder.to_dict(), response_builder.code)

    def destroy(self, request, pk):
        with api_exception_handler() as response_builder:
            kategori = self.queryset.filter(id=pk).first()
            if not kategori:
                raise FileNotFoundError('Kategori not found!')

            kategori.delete()

        return Response(response_builder.to_dict(), response_builder.code)
